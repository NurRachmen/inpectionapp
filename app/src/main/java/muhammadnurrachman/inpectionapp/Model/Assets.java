package muhammadnurrachman.inpectionapp.Model;

public class Assets {
    private Integer QRCodeNumber;
    private String Plant, AssetNumber, AssetDesc, Location;
    private Boolean isFound;

    public Assets() {
    }

    public Assets(Integer QRCodeNumber, String plant, String assetNumber, String assetDesc, String location) {
        this.QRCodeNumber = QRCodeNumber;
        Plant = plant;
        AssetNumber = assetNumber;
        AssetDesc = assetDesc;
        Location = location;
    }

    public Integer getQRCodeNumber() {
        return QRCodeNumber;
    }

    public void setQRCodeNumber(Integer QRCodeNumber) {
        this.QRCodeNumber = QRCodeNumber;
    }

    public String getPlant() {
        return Plant;
    }

    public void setPlant(String plant) {
        Plant = plant;
    }

    public String getAssetNumber() {
        return AssetNumber;
    }

    public void setAssetNumber(String assetNumber) {
        AssetNumber = assetNumber;
    }

    public String getAssetDesc() {
        return AssetDesc;
    }

    public void setAssetDesc(String assetDesc) {
        AssetDesc = assetDesc;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        Location = location;
    }

    public Boolean getFound() {
        return isFound;
    }

    public void setFound(Boolean found) {
        isFound = found;
    }
}
