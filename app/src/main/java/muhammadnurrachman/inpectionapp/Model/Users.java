package muhammadnurrachman.inpectionapp.Model;

public class Users {
    private String ID, Name, Supervising, SupervisingDescription, Company, InspectionRegistered, Username, Password;

    public Users() {
    }

    public Users(String ID, String name, String supervising, String supervisingDescription, String company, String inspectionRegistered, String username, String password) {
        this.ID = ID;
        Name = name;
        Supervising = supervising;
        SupervisingDescription = supervisingDescription;
        Company = company;
        InspectionRegistered = inspectionRegistered;
        Username = username;
        Password = password;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getSupervising() {
        return Supervising;
    }

    public void setSupervising(String supervising) {
        Supervising = supervising;
    }

    public String getSupervisingDescription() {
        return SupervisingDescription;
    }

    public void setSupervisingDescription(String supervisingDescription) {
        SupervisingDescription = supervisingDescription;
    }

    public String getCompany() {
        return Company;
    }

    public void setCompany(String company) {
        Company = company;
    }

    public String getInspectionRegistered() {
        return InspectionRegistered;
    }

    public void setInspectionRegistered(String inspectionRegistered) {
        InspectionRegistered = inspectionRegistered;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }
}
