package muhammadnurrachman.inpectionapp.View.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import muhammadnurrachman.inpectionapp.Database.SQLiteHelper;
import muhammadnurrachman.inpectionapp.R;
import muhammadnurrachman.inpectionapp.SharedPreferences.Helper;

public class Login extends AppCompatActivity {
    SQLiteHelper sqLiteHelper;
    EditText username, password;
    Button login;
    Helper helper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        sqLiteHelper = new SQLiteHelper(this);
        helper = new Helper(this);

        username = findViewById(R.id.username);
        password = findViewById(R.id.password);
        login = findViewById(R.id.login);

        initEvent();
    }

    private void initEvent(){
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String usernameInput = username.getText().toString(), passwordInput = password.getText().toString();
                if (!usernameInput.isEmpty() && !passwordInput.isEmpty()){
                    if (sqLiteHelper.login(usernameInput, passwordInput)){
                        helper.isLogin(true);
                        Intent i = new Intent(Login.this, MainMenu.class);
                        startActivity(i);
                        finish();
                        return;
                    }
                    helper.isLogin(false);
                    username.setText(null);
                    password.setText(null);
                    Toast.makeText(Login.this, "Something Wrong!", Toast.LENGTH_SHORT).show();
                    return;
                }
                Toast.makeText(Login.this, "Required", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
