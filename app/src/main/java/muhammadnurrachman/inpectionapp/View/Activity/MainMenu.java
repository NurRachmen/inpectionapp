package muhammadnurrachman.inpectionapp.View.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

import java.util.Objects;

import muhammadnurrachman.inpectionapp.R;
import muhammadnurrachman.inpectionapp.View.Fragment.Profile;
import muhammadnurrachman.inpectionapp.View.Fragment.ScanQR;

public class MainMenu extends AppCompatActivity {

    private String scanTAG = "Scan", profileTAG = "Profile",currentTAG = scanTAG;
    private Integer index = 0;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_find_item: index = 0; currentTAG = scanTAG; break;
                case R.id.navigation_profile: index = 1; currentTAG = profileTAG; break;
            }
            doChangeFragment(index);
            return true;
        }
    };
    private BottomNavigationView.OnNavigationItemReselectedListener onNavigationItemReselectedListener = new BottomNavigationView.OnNavigationItemReselectedListener() {
        @Override
        public void onNavigationItemReselected(@NonNull MenuItem menuItem) {

        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.setOnNavigationItemReselectedListener(onNavigationItemReselectedListener);
        doChangeFragment(index);
    }
    public void doChangeFragment(Integer position){
        Objects.requireNonNull(getSupportActionBar()).setTitle(currentTAG);
        Fragment fragment;
        switch (position){
            case 0:
                fragment = new ScanQR();
                break;
            case 1:
                fragment = new Profile();
                break;
            default: fragment = new ScanQR();
        }
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        fragmentTransaction.replace(R.id.parentLayout, fragment, currentTAG);
        fragmentTransaction.commit();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

}
