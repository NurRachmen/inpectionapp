package muhammadnurrachman.inpectionapp.View.Activity;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import muhammadnurrachman.inpectionapp.R;
import muhammadnurrachman.inpectionapp.SharedPreferences.Helper;

public class Splashscreen extends AppCompatActivity {
    private Helper helper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);
        helper = new Helper(this);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (helper.isLogin()){
                    Intent i = new Intent(Splashscreen.this, MainMenu.class);
                    startActivity(i);
                    finish();
                    return;
                }
                Intent i = new Intent(Splashscreen.this, Login.class);
                startActivity(i);
                finish();
            }
        }, 3000);

    }
}
