package muhammadnurrachman.inpectionapp.View.Fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.Objects;

import muhammadnurrachman.inpectionapp.Database.SQLiteHelper;
import muhammadnurrachman.inpectionapp.Model.Users;
import muhammadnurrachman.inpectionapp.R;
import muhammadnurrachman.inpectionapp.SharedPreferences.Helper;
import muhammadnurrachman.inpectionapp.View.Activity.Login;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Profile.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Profile#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Profile extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public Profile() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Profile.
     */
    // TODO: Rename and change types and number of parameters
    public static Profile newInstance(String param1, String param2) {
        Profile fragment = new Profile();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }
    View rootView;
    TextView userId, userName, userSupervising, userSupervisingDescription, userCompany, userInspectionRegistered;
    Helper helper;
    SQLiteHelper sqLiteHelper;
    Button logout;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        helper = new Helper(Objects.requireNonNull(getContext()));
        sqLiteHelper = new SQLiteHelper(getContext());

        userId = rootView.findViewById(R.id.userId);
        userName = rootView.findViewById(R.id.userName);
        userCompany = rootView.findViewById(R.id.userCompany);
        userSupervising = rootView.findViewById(R.id.userSupervising);
        userSupervisingDescription = rootView.findViewById(R.id.userSupervisingDescription);
        userInspectionRegistered = rootView.findViewById(R.id.userInspectionRegistered);
        logout = rootView.findViewById(R.id.logout);

        displayData();
        initEvent();

        return rootView;
    }

    private void displayData(){
        Users users = sqLiteHelper.dataLogin(helper.getIdLogin());
        userId.setText(users.getID());
        userName.setText(users.getName());
        userCompany.setText(users.getCompany());
        userSupervising.setText(users.getSupervising());
        userSupervisingDescription.setText(users.getSupervisingDescription());
        userInspectionRegistered.setText(users.getInspectionRegistered());
    }

    private void initEvent(){
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                helper.isLogin(false);
                Intent i = new Intent(getContext(), Login.class);
                startActivity(i);
                Objects.requireNonNull(getActivity()).finish();
            }
        });
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }
//
//    @Override
//    public void onDetach() {
//        super.onDetach();
//        mListener = null;
//    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
