package muhammadnurrachman.inpectionapp.SharedPreferences;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

public class Helper {
    private Context context;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    @SuppressLint("CommitPrefEdits")
    public Helper(Context context) {
        this.context = context;
        sharedPreferences = context.getSharedPreferences(
                "inpectionapp.sharedPreferences", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }
    public void isLogin(Boolean login){
        editor.putBoolean("login", login);
        editor.commit();
    }
    public void storeIdLogin(String id){
        editor.putString("idLogin", id);
        editor.commit();
    }
    public String getIdLogin(){ return sharedPreferences.getString("idLogin", "");}
    public Boolean isLogin(){ return sharedPreferences.getBoolean("login", false); }
}
