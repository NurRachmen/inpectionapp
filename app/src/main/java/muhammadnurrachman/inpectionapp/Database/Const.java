package muhammadnurrachman.inpectionapp.Database;

class Const {
    static String databaseName = "databaseName";
    static Integer databaseVersion = 1;

    static String tableUser = "tableUser";
    static String userID = "userID";
    static String userName = "userName";
    static String userSupervising = "userSupervising";
    static String userSupervisingDescription = "userSupervisingDescription";
    static String userCompany = "userCompany";
    static String userInspectionRegisted = "userInspectionRegisted";
    static String userUsername = "userUsername";
    static String userPassword = "userPassword";

    static String tableAsset = "tableAsset";
    static String assetQRCodeNumber = "assetQRCodeNumber";
    static String assetPlant = "assetPlant";
    static String assetNumber = "assetNumber";
    static String assetDesc = "assetDesc";
    static String assetLocation = "assetLocation";
}
