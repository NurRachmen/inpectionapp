package muhammadnurrachman.inpectionapp.Database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import muhammadnurrachman.inpectionapp.Model.Assets;
import muhammadnurrachman.inpectionapp.Model.Users;
import muhammadnurrachman.inpectionapp.SharedPreferences.Helper;

public class SQLiteHelper {
    private DatabaseSQLite database;
    private SQLiteDatabase sqLiteDatabase;
    private Cursor cursor;
    private Helper helper;

    public SQLiteHelper(Context context) {
        database = new DatabaseSQLite(context);
        helper = new Helper(context);
    }

    public Boolean login(String username, String password){
        sqLiteDatabase = database.getReadableDatabase();
        cursor = sqLiteDatabase.rawQuery("SELECT * FROM " + Const.tableUser + " WHERE Username = '" + username + "' and Password = '" + password + "'", null);
        cursor.moveToFirst();
        int cursorCount = cursor.getCount();
        if (cursorCount > 0){
            helper.storeIdLogin(cursor.getString(0));
        }
        return cursorCount > 0;
    }

    public Users dataLogin(String id){
        sqLiteDatabase = database.getReadableDatabase();
        cursor = sqLiteDatabase.rawQuery("SELECT * FROM " + Const.tableUser + " WHERE ID = '" + id + "'", null);
        cursor.moveToFirst();
        Users users = new Users();
        users.setID(cursor.getString(0));
        users.setName(cursor.getString(1));
        users.setSupervising(cursor.getString(2));
        users.setSupervisingDescription(cursor.getString(3));
        users.setCompany(cursor.getString(4));
        users.setInspectionRegistered(cursor.getString(5));
        return users;
    }

    public Assets dataQR(String qrNumber){
        sqLiteDatabase = database.getReadableDatabase();
        cursor = sqLiteDatabase.rawQuery("SELECT * FROM " + Const.tableAsset + " WHERE QRCodeNumber = '" + qrNumber + "'", null);
        cursor.moveToFirst();
        int cursorCount = cursor.getCount();
        Assets assets = new Assets();
        if (cursorCount > 0){
            assets.setFound(true);
            assets.setQRCodeNumber(cursor.getInt(0));
            assets.setPlant(cursor.getString(1));
            assets.setAssetNumber(cursor.getString(2));
            assets.setAssetDesc(cursor.getString(3));
            assets.setLocation(cursor.getString(4));
            return assets;
        }
        assets.setFound(false);
        return assets;
    }

}
