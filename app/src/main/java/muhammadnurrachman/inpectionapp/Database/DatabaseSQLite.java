package muhammadnurrachman.inpectionapp.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static muhammadnurrachman.inpectionapp.Database.Const.databaseName;
import static muhammadnurrachman.inpectionapp.Database.Const.databaseVersion;
import static muhammadnurrachman.inpectionapp.Database.Const.tableAsset;
import static muhammadnurrachman.inpectionapp.Database.Const.tableUser;

public class DatabaseSQLite extends SQLiteOpenHelper {

    DatabaseSQLite(Context context){
        super(context, databaseName, null, databaseVersion);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql;
        sql = "CREATE TABLE " + Const.tableUser + "( " +
                "ID Integer ," +
                "Name text," +
                "Supervising text," +
                "SupervisingDescription text," +
                "Company text," +
                "InspectionRegistered text," +
                "Username text," +
                "Password text)";
        db.execSQL(sql);

        sql = "CREATE TABLE " + Const.tableAsset + "( " +
                "QRCodeNumber Integer PRIMARY KEY," +
                "Plant text," +
                "AssetNumber text," +
                "AssetDesc text," +
                "Location text)";
        db.execSQL(sql);

        sql = "INSERT or replace INTO "+ tableUser +" (ID, Name, Supervising, SupervisingDescription,Company,InspectionRegistered,Username,Password) " +
                "VALUES('1','User','UserSupervising','userSuperVisingDescription','userCompany', 'userInspectionRegistered', 'user', 'user1234')";
        db.execSQL(sql);
        sql = "INSERT or replace into " + tableAsset + "(QRCodeNumber, Plant, AssetNumber, AssetDesc, Location) " +
                "VALUES(1, 'Plant1', 'AssetNumber1', 'AssetDesc1','assetLocation1')";
        db.execSQL(sql);
        sql = "INSERT or replace into " + tableAsset + "(QRCodeNumber, Plant, AssetNumber, AssetDesc, Location) " +
                "VALUES(2, 'Plant2', 'AssetNumber2', 'AssetDesc2','assetLocation2')";
        db.execSQL(sql);
        sql = "INSERT or replace into " + tableAsset + "(QRCodeNumber, Plant, AssetNumber, AssetDesc, Location) " +
                "VALUES(3, 'Plant3', 'AssetNumber3', 'AssetDesc3','assetLocation3')";
        db.execSQL(sql);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
